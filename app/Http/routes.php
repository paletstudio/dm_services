<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::group(['prefix' => 'api'], function () {
    Route::post('login', 'UserController@login');
    Route::post('register', 'UserController@register');
    Route::post('logout', 'UserController@logout');

    Route::get('user/{id}/picture', 'UserController@getProfilePicture')->where('id', '[0-9]+');
    Route::get('refreshtoken', 'UserController@refreshToken');
    Route::post('user/forgotpass', 'UserController@forgotPass');
    Route::get('user/changepass', 'UserController@sendNewPass');
	Route::get('course/{course_id}/certificate/user/{id}', 'DiplomaController@downloadCertificate');

    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::post('course/{course_id}/certificate/user/{id}', 'DiplomaController@generateCertificate');
        Route::post('notification', 'NotificationController@save');

        Route::post('exam/{exam_id}/grade', 'ExamController@evaluateExam')->where('exam_id', '[0-9]+');
        Route::get('exam/pending', 'ExamController@pendingExams');

        Route::group(['prefix' => 'user'], function () {
            Route::get('', 'UserController@getAll');
            Route::get('{id}', 'UserController@get')->where('id', '[0-9]+');
            Route::post('{id}/picture', 'UserController@changeProfilePicture')->where('id', '[0-9]+');
            Route::post('{id}/sendinvitation', 'UserController@sendInvitation')->where('id', '[0-9]+');
            Route::patch('{id}/password', 'UserController@changePassword')->where('id', '[0-9]+');
            Route::get('{id}/image/{imagename}', 'UserController@getImage')->where('id', '[0-9]+');
            Route::get('{id}/video/{video}', 'UserController@getVideo')->where('id', '[0-9]+');
			Route::delete('{id}', 'UserController@deleteUser');
        });

        Route::group(['prefix' => 'answer'], function () {
            Route::post('{id}', 'ExamController@evaluateAnswer');
        });

        Route::group(['prefix' => 'course'], function () {
            Route::get('', 'CourseController@getAll');
            Route::get('{course_id}', 'CourseController@get');
            //post un nuevo curso
            //get bloque
            Route::group(['prefix' => '{course_id}/block'], function () {
                Route::get('', 'BlockController@getAll');
                Route::get('{block_id}', 'BlockController@get');
                Route::post('{block_id}/unlock', 'BlockController@unlockAccess');
                Route::post('{block_id}/done', 'BlockController@doneAccess');

                //post un nuevo bloque
                //get modulo
                //
                Route::group(['prefix' => '{block_id}/module'], function () {
                    Route::get('', 'ModuleController@getAll');
                    Route::get('{module_id}', 'ModuleController@get');
                    Route::post('{module_id}/exam', 'ExamController@saveExamAttempt');
                    Route::post('{module_id}/exam/video', 'ExamController@saveVideo');
                    //post un nuevo modulo
                    //get contenido
                    Route::group(['prefix' => '{module_id}/content'], function () {
                        Route::get('', 'ContentController@getAll');
                        Route::get('{content_id}', 'ContentController@get');
                        Route::post('{content_id}/pdf', 'ContentController@sendPDF');
                        Route::get('{content_id}/pdf', 'ContentController@viewPDF');
                        Route::post('{content_id}/validate', 'ContentController@validateAnswer');
                    });
                });
            });
        });
    });
});
