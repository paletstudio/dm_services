<?php

namespace App\Http\Controllers;

use Request;
use Response;

class BlockController extends Controller
{
    public function get($course_id, $block_id)
    {
        $user_id = Request::get('user_id');
        $response = \App\Block::get($course_id, $block_id, $user_id);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function getAll($course_id)
    {
        $user_id = request('user_id', null);
        $response = \App\Block::getAll($course_id, $user_id);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function unlockAccess($course_id, $block_id)
    {
        $response = new \App\Response();
        $user_id = Request::get('user_id');

        try {
            \App\UserAccess::unlockAccess(2, $block_id, $user_id);
            $response->code = 200;
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
        }

        return response()->json($response)->setStatusCode($response->code);
    }

    public function doneAccess($course_id, $block_id)
    {
        $response = new \App\Response();
        $user_id = Request::get('user_id');

        try {
            \App\UserAccess::doneAccess(2, $block_id, $user_id);
            $response->code = 200;
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
        }

        return response()->json($response)->setStatusCode($response->code);
    }
}
