<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Storage;
use File;

class ExamController extends Controller
{
    public function saveExamAttempt(Request $request, $course_id, $block_id, $module_id)
    {
        $response = \App\ExamAttempt::saveExam($course_id, $block_id, $module_id, $request->all());
        return response()->json($response)->setStatusCode($response->code);
    }

    public static function saveVideo(Request $request, $course_id, $block_id, $module_id)
    {
        if ($request->file('video')) {
            // $video = $request->file('video');
            $params = $request->only(['user_id', 'question_id']);
            $response = \App\ExamAttempt::saveVideo($course_id, $block_id, $module_id, $params);

            return response()->json($response)->setStatusCode($response->code);
        } else {
            //bad request
            return response()->json(['respuesta' => $request->input('question_id')]);
        }
    }

    public function evaluateAnswer(Request $request, $id)
    {
        $grade = $request->input('grade');
        $response = \App\ExamAnswer::evaluateAnswer($id, $grade);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function evaluateExam(Request $request, $exam_id)
    {
        $grade = $request->input('grade');
        $exam = $request->input('exam');

        $block_id = $request->get('block_id');
        $module_id = $request->get('module_id');

        $response = \App\ExamAttempt::evaluateExam($exam_id, $grade, $exam, $block_id, $module_id);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function pendingExams()
    {
        $response = \App\ExamAttempt::pendingExams();
        return response()->json($response)->setStatusCode($response->code);
    }
}
