<?php

namespace App\Http\Controllers;

use Request;
use Response;

class DiplomaController extends Controller
{
    public function generateCertificate($course_id, $user_id)
    {
        $response = \App\Diploma::generateCertificate($course_id, $user_id);
        return response()->json($response)->setStatusCode($response->code);
    }

	public function downloadCertificate($course_id, $id)
	{
		$response = \App\Diploma::getUrl($course_id, $id);
		if(file_exists($response)){
			return response()->download($response, 'Draught_Master_Certificate.pdf');
		} else {
			return view('errors.404');
		}
	}
}
