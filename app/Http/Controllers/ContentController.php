<?php

namespace App\Http\Controllers;

use Request;
use Response;

class ContentController extends Controller
{
    // public function get($course_id, $block_id, $module_id)
    // {
    //     $response = \App\Content::get($course_id, $block_id, $module_id);
    //     return response()->json($response)->setStatusCode($response->code);
    // }

    public function getAll($course_id, $block_id, $module_id)
    {
        $only_exams = Request::get('only_exams');
        $user_id = Request::get('user_id');
        $response = \App\Content::getAll($course_id, $block_id, $module_id, $only_exams, $user_id);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function get($course_id, $block_id, $module_id, $content_id)
    {
        $response = \App\Content::get($course_id, $block_id, $module_id, $content_id);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function sendPDF($course_id, $block_id, $module_id, $content_id)
    {
        $email = Request::get('email');
        $response = \App\Content::sendPDF($email, $course_id, $block_id, $module_id, $content_id);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function viewPDF($course_id, $block_id, $module_id, $content_id)
    {
        $response = \App\Content::viewPDF($course_id, $block_id, $module_id, $content_id);
        return response()->file($response);
    }

	public function validateAnswer($course_id, $block_id, $module_id, $content_id)
	{
		$user_id = Request::get('user_id');
		$question_id = Request::get('question_id');
		$response = \App\Content::validateAnswer($content_id, $user_id, $question_id);
		return response()->json($response)->setStatusCode($response->code);
	}
}
