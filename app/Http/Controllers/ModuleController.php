<?php

namespace App\Http\Controllers;

use Request;
use Response;

class ModuleController extends Controller
{
    public function get($course_id, $block_id, $module_id)
    {
        $user_id = Request::get('user_id');
        $response = \App\Module::get($course_id, $block_id, $module_id, $user_id);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function getAll($course_id, $block_id)
    {
        $user_id = Request::get('user_id');
        $response = \App\Module::getAll($course_id, $block_id, $user_id);
        return response()->json($response)->setStatusCode($response->code);
    }
}
