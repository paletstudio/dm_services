<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use JWTAuth;

class NotificationController extends Controller
{
    public function save(Request $request)
    {
        $params = $request->all();
        if($token = JWTAuth::parseToken()){
            $payload = $token->getPayload();

            if(!$payload['user'] || !$payload['user']['id']){
                return response()->json(new \App\Response(400, 'Token malformado'))->setStatusCode(400);
            }
            $params['user_id'] = $payload['user']['id'];
        }
        $response = \App\Notification::saveNotification($params);
        return response()->json($response)->setStatusCode($response->code);
    }
}
