<?php

namespace App\Http\Controllers;

use Request;
use Response;

class CourseController extends Controller
{
    public function get($course_id)
    {
        $user_id = Request::get('user_id');
        $response = \App\Course::get($course_id, $user_id);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function getAll()
    {
        $user_id = Request::get('user_id');
        $response = \App\Course::getAll($user_id);
        return response()->json($response)->setStatusCode($response->code);
    }
}
