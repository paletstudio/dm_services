<?php

namespace App\Http\Controllers;

// use Request;
use Illuminate\Http\Request;
use Response;
use JWTAuth;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password', 'origin', 'pushToken');
        $credentials['origin'] = $credentials['origin'] ? $credentials['origin'] : 1;
        $response = \App\User::login($credentials);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function register(Request $request)
    {
        $user = $request->all();
        $response = \App\User::register($user);

        return response()->json($response)->setStatusCode($response->code);
    }
    public function refreshtoken()
    {
        $response = \App\User::refreshToken();
        return response()->json($response)->setStatusCode($response->code);
    }

    public function get($id)
    {
        $response = \App\User::get($id);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function getImage($id, $imagename)
    {
        $response = \App\User::getImage($id, $imagename);

        return response($response->image)->header('Content-Type', $response->mime);
    }

    public function getVideo($id, $video)
    {
        $response = \App\User::getVideo($id, $video);
        if ($response->video) {
            $file = $response->video;
            $mime = 'video/mp4';
            $size = filesize($file);
            $length = $size;
            $start = 0;
            $end = $size - 1;

            header(sprintf('Content-type: %s', $mime));
            header('Accept-Ranges: bytes');

            if (isset($_SERVER['HTTP_RANGE'])) {
                $c_start = $start;
                $c_end = $end;

                list(, $range) = explode('=', $_SERVER['HTTP_RANGE'], 2);

                if (strpos($range, ',') !== false) {
                    header('HTTP/1.1 416 Requested Range Not Satisfiable');
                    header(sprintf('Content-Range: bytes %d-%d/%d', $start, $end, $size));

                    exit;
                }

                if ($range == '-') {
                    $c_start = $size - substr($range, 1);
                } else {
                    $range = explode('-', $range);
                    $c_start = $range[0];
                    $c_end = (isset($range[1]) && is_numeric($range[1])) ? $range[1] : $size;
                }

                $c_end = ($c_end > $end) ? $end : $c_end;

                if ($c_start > $c_end || $c_start > $size - 1 || $c_end >= $size) {
                    header('HTTP/1.1 416 Requested Range Not Satisfiable');
                    header(sprintf('Content-Range: bytes %d-%d/%d', $start, $end, $size));

                    exit;
                }

                header('HTTP/1.1 206 Partial Content');

                $start = $c_start;
                $end = $c_end;
                $length = $end - $start + 1;
            }

            header("Content-Range: bytes $start-$end/$size");
            header(sprintf('Content-Length: %d', $length));

            $fh = fopen($file, 'rb');
            $buffer = 1024 * 8;

            fseek($fh, $start);

            while (true) {
                if (ftell($fh) >= $end) {
                    break;
                }

                set_time_limit(0);

                echo fread($fh, $buffer);

                flush();
            }
        } else {
            return response()->json($response)->setStatusCode($response->code);
        }
    }

    public function getAll(Request $req)
    {
        $info = $req->get('info');
        $response = \App\User::getAll($info);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function logout()
    {
        $response = new \App\Response();
        $token = JWTAuth::getToken();
        if ($token) {
            $payload = JWTAuth::decode($token);
            $user = \App\User::find($payload['user']['id']);
            if($user){
                $user->push_notif_token = null;
                $user->save();
            }
            if (JWTAuth::invalidate($token)) {
                $response->code = 200;
                $response->msg = 'Logout con éxito.';
            }
        }

        return response()->json($response)->setStatusCode($response->code);
    }
    //
    // public function refreshtoken()
    // {
    //     $response = \App\User::refreshToken();
    //
    //     return response()->json($response)->setStatusCode($response->code);
    // }

    public function getProfilePicture($id)
    {
        $response = \App\User::getProfileImage($id);

        if($response->image) {
            return response($response->image)->header('Content-Type', $response->mime);
        }

        return response()->json()->setStatusCode(404);


    }

    public function changeProfilePicture(Request $req, $id)
    {
        $image = $req->get('profile_pic');
        $response = \App\User::changeProfilePicture($id, $image);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function changePassword(Request $req, $id)
    {
        $oldPass = $req->get('password');
        $newPass = $req->get('new_password');
        $response = \App\User::changePassword($id, $oldPass, $newPass);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function sendInvitation($id)
    {
        $response = \App\User::sendInvitation($id);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function forgotPass(Request $request)
    {
      $email = $request->only('email');
      $response = \App\User::forgotPass($email);
      return response()->json($response)->setStatusCode($response->code);
    }

    public function sendNewPass(Request $request)
    {
		$id = $request->get('user_id');

		try {
			$token = JWTAuth::getToken();
			$payload = JWTAuth::decode($token);

			$now = time();
			$expires = (int) $payload['exp'];
			if ($now < $expires) {
			  $response = \App\User::sendNewPass($id);
			  if ($response->code == 200) {
				JWTAuth::invalidate($token);
				return view('success.changepass');
			  }
			} else {
				return view('errors.404');
			}
		} catch(\Exception $e) {
			return view('errors.404');
		}
    }

	public function deleteUser($id)
	{
		$response = \App\User::deleteUser($id);
		return response()->json($response)->setStatusCode($response->code);
	}
}
