<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;
use Storage;
use File;
use Request;
use DB;

class ExamAttempt extends Model
{
    protected $table = "exam_attempt";

    public function answer()
    {
        return $this->hasOne('App\ExamAnswer');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }

    /**
     * Set a grade for a exam attempt.
     * @param int $attempt_id Exam attempt id.
     * @param int $grade Grade of the exam.
     * @return Response
     */
    public static function setGrade($attempt_id, $grade)
    {
        $response = new Response();
        try {
            $attempt = ExamAttempt::find($attempt_id);
            $attempt->grade = $grade;
            $attempt->save();
            $response->code = 200;
        }
        catch (\Exception $e) {
            $response->code = 200;
            $response->exception = $e->getMessage();
            $response->msg = "Se produjo un error";
        }
        return $response;
    }

    /**
     * Saves an exam and save exam answers. Evaluate each answer if possible.
     * @param  int $course_id Course id.
     * @param  int $block_id  Block id.
     * @param  int $module_id Module id.
     * @param  array $attempt   The entire exam and answers.
     * @return Response
     */
    public static function saveExam($course_id = null, $block_id = null, $module_id = null, array $attempt = [])
    {
        $response = new Response();
        $self_grade = true;
        $grades = [];

        try {
            $newAttempt = new ExamAttempt;
            $newAttempt->grade = NULL;
            $newAttempt->user_id = $attempt['user_id'];
            if ($newAttempt->save()){
                foreach($attempt['questions'] as $q) {
                    //TODO: Mover a funcion
                    if ($q['exam_answer_type_id'] === 1) {
                        $answer = new ExamAnswer;
                        $answer->answer = $q['answer'];
                        $answer->exam_attempt_id = $newAttempt->id;
                        $answer->exam_question_id = $q['id'];
                        $answer->grade = 0;
                        $answer->save();
                        $self_grade = false;
                    } elseif ($q['exam_answer_type_id'] === 2) {
                        $answer = new ExamAnswer;
                        $answer->answer = $q['answer'];
                        $answer->exam_attempt_id = $newAttempt->id;
                        $answer->exam_question_id = $q['id'];
                        if (intval($q['correct_answer_id']) === $q['answer']) {
                            $answer->grade = 100;
                        } else {
                            $answer->grade = 0;
                        }
                        array_push($grades, $answer->grade);
                        $answer->save();
                    } elseif ($q['exam_answer_type_id'] === 3) {
                        $answer = new ExamAnswer;
                        $answer->answer = self::saveImage($q, $attempt['user_id'], $course_id, $block_id, $module_id);
                        $answer->exam_attempt_id = $newAttempt->id;
                        $answer->exam_question_id = $q['id'];
                        $answer->save();
                        $self_grade = false;
                    }
                }
                $response->data = $newAttempt->id;

                if ($self_grade) {
                    $exam_grade = 0;
                    foreach($grades as $grade){
                        $exam_grade += $grade;
                    }
                    $length = count($grades);
                    $exam_grade = $exam_grade/$length;
                    $newAttempt->grade = $exam_grade;
                    $newAttempt->save();

                    $response->data = new \stdClass;
                    $response->data->exam_attempt_id = $newAttempt->id;
                    $response->data->grade = $exam_grade;
                }
                UserAccess::unblockAccess($course_id, $block_id, $module_id, $attempt['user_id']);
                $response->code = 200;
                $response->msg = "Quiz enviado correctamente";
            }

        }
        catch (\Exception $e) {
            $response->code = 500;
            $response->exception = $e->getMessage(). ' '. $e->getLine();
            $response->msg = "Se produjo un error";
        }
        return $response;
    }

    /**
     * Saves an image to storage.
     * @param  array  $q         [description]
     * @param  int $user_id   User id.
     * @param  int $course_id Course id.
     * @param  int $block_id  Block id.
     * @param  int $module_id Module id.
     * @return string            Path to the saved image.
     */
    private static function saveImage(array $q, $user_id, $course_id, $block_id, $module_id)
    {
        $img = Image::make($q['answer']);
        $mime = $img->mime();
        $img->stream();
        if ($mime == 'image/jpeg') {
            $ext = '.jpg';
        } else if ($mime == 'image/png') {
            $ext = '.png';
        } else if ($mime == 'image/gif') {
            $ext = '.gif';
        }
        $filePath = 'user/'.$user_id.'/course/c'.$course_id.'_b'.$block_id.'_m'.$module_id.'_q'.$q['id'].'_'.time().$ext;
        Storage::put($filePath, $img, 'public');
        return $filePath;
    }

    /**
     * Save a video as an answer to an exam question.
     * @param  mixed $course_id Course id.
     * @param  mixed $block_id  Block id.
     * @param  mixed $module_id Module id.
     * @param  mixed $exam_id   Exam id.
     * @param  array $params    Params.
     * @param  mixed $video     Video to upload.
     * @return Response            Response.
     */
    public static function saveVideo($course_id, $block_id, $module_id, array $params)
    {
        $response = new Response();
        try {
            $newAttempt = new ExamAttempt;
            $newAttempt->grade = NULL;
            $newAttempt->user_id = $params['user_id'];
			if ($module_id == 15 || $module_id == 16) {
				$newAttempt->grade = 100;
			}
            $newAttempt->save();
            $video = Request::file('video');
            $extension = $video->getClientOriginalExtension();
            $filePath = 'user/'.$params['user_id'].'/course/c'.$course_id.'_b'.$block_id.'_m'.$module_id.'_q'.$params['question_id'].'_'.time().'.'.$extension;
            Storage::put($filePath, File::get($video), 'public');

            $answer = new ExamAnswer;
            $answer->answer = $filePath;
            $answer->exam_attempt_id = $newAttempt->id;
            $answer->exam_question_id = $params['question_id'];
            $answer->save();

            UserAccess::unblockAccess($course_id, $block_id, $module_id, $params['user_id']);

			// Se verifica si el módulo contestado es el último y se califica automáticamente el ultimo bloque
			if($course_id == 1 && $block_id == 5 && $module_id == 16){
				UserAccess::doneAccess(2, $block_id, $params['user_id']);
			}

            $response->msg = "Quiz enviado correctamente";

            $response->code = 200;
            $response->data = true;
        }
        catch (\Exception $e) {
            $response->code = 500;
            $response->msg = 'Se produjo un error';
            $response->exception = $e->getMessage() . ' ' . $e->getLine() . ' ' . $e->getFile();
            $response->data = false;
        }
        return $response;

    }

    /**
     * Set grade for an existing exam.
     * @param  int    $exam_id Exam id.
     * @param  int    $grade   Grate to assign.
     * @param  array  $exam    Exam answers.
     * @return Response
     */
    public static function evaluateExam($exam_id, $grade, $exam, $block_id = null, $module_id = null)
    {
        $response = new Response();
        try {
            foreach($exam as $answer) {
                if ($answer['exam_answer_type_id'] != 2) {
                    $newAnswer = ExamAnswer::where([
                        ['exam_question_id', '=', $answer['answer']['exam_question_id']],
                        ['exam_attempt_id', '=', $answer['answer']['id']]
                    ])
                    ->first();
                    $newAnswer->grade = $answer['grade'];
                    $newAnswer->save();
                }
            }
            $exam = ExamAttempt::find($exam_id);

            if($exam) {
                $exam->grade = $grade;
                $exam->save();

                /**
                * Logica para desbloquear contenidos del bloque 4 y 5
                */
                if($block_id && $module_id && $grade === 100) {
                    $module = Module::find($module_id);

                    if($module) {
                        //Si es el bloque 3 y es el examen del modulo 9
                        //entonces se desbloquea el bloque 4
                        if($block_id == 3 && $module->order == 9) {
                            UserAccess::unlockAccess(2, 4, $exam->user_id);
                        } else if ($block_id == 5 && $module->order == 1) { //Si es el bloque 5 y el examen
                            $nextModule = Module::where([['block_id', '=', 5], ['order', '=', 2]])->first();
                            if($nextModule){
                                UserAccess::unlockAccess(3, $nextModule->id, $exam->user_id);
                            }
                        }
                    }
                }

                $response->code = 200;
                $response->msg = "Se calificó el exámen correctamente";
            }
        }
        catch (\Exception $e) {
            $response->code = 500;
            $response->exception = $e->getMessage().' '.$e->getLine();
            $response->msg = "Se produjo un error";
        }

        return $response;
    }

    /**
     * Get pending exams
     * @return Response
     */
    public static function pendingExams()
    {
        $response = new Response;

        try {
            $response->data = DB::select("SELECT u.id as user_id, u.name as user_name, u.lastname as user_lastname, c.module_id as module_id, m.name as module_name, ea.id as attempt,m.block_id as block_id
            FROM exam_attempt as ea
            INNER JOIN user as u ON ea.user_id = u.id
            INNER JOIN user_exam_answer as a ON a.exam_attempt_id = ea.id
            INNER JOIN exam_question as q ON q.id = a.exam_question_id
            INNER JOIN content as c ON c.id = q.content_id
            INNER JOIN module as m ON m.id = c.module_id
            WHERE ea.grade is null AND ea.id in (SELECT max(id) FROM `exam_attempt` group by user_id)
            Group by c.module_id, ea.id");
            $response->code = 200;
        }
        catch (\Exception $e) {
            $response->code = 500;
            $response->msg = "Se produjo un error";
            $response->exception = $e->getMessage();
        }
        return $response;

    }
}
