<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Block extends Model
{
    protected $table = 'block';

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    public function access()
    {
        return $this->hasOne('App\UserAccess', 'object_id')->where('access_type_id', 2);
    }

    public function modules()
    {
        return $this->hasMany('App\Module');
    }

    public static function get($course_id, $block_id, $user_id = null)
    {
        $response = new Response();

        try {
            $block = Block::where([
                ['id', $block_id],
                ['course_id', $course_id]
            ]);

            if($user_id){
                $block = $block->with(['access' => function($query) use ($user_id){
                    $query->where('user_id', $user_id);
                }])
                ->get(['block.*', DB::raw('(SELECT GetBlockGrade(block.id, '.$user_id.')) as grade')])->first();
            } else {
                $block = $block->first();
            }

            $response->data = $block;
            $response->code = 200;
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
        }

        return $response;

    }

    public static function getAll($course_id, $user_id = null)
    {
        $response = new Response();

        try {
            $blocks = Block::where([
                ['course_id', $course_id],
                ['active', 1]
            ])
            ->orderBy('order');

            if($user_id){
                $blocks = $blocks->with(['access' => function($query) use ($user_id){
                    $query->where('user_id', $user_id);
                }])
                ->get(['block.*', DB::raw('(SELECT GetBlockGrade(block.id, '.$user_id.')) as grade')]);

                //Logica para cambiar calificacion del bloque dependiendo de la propiedad DONE de UserAccess
                foreach ($blocks as $key => $block) {
                    if($block->grade == -1 && $block->access->done == 1){
                        unset($block->grade);
                        $block->grade = 100;
                    }
                }
            } else {
                $blocks = $blocks->get();
            }

            $response->data = $blocks;
            $response->code = 200;
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
        }

        return $response;

    }
}
