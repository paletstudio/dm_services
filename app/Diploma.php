<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;
use Image;
use PDF;
use Dompdf\Adapter\CPDF;

class Diploma extends Model
{
    protected $table = "diploma";

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function generateCertificate($course_id, $user_id)
    {
        $response = new Response();

        try {
            $course = Course::find($course_id);
            $user = User::find($user_id);

            if($course && $user) {
                $diploma = Diploma::where([['course_id', $course_id], ['user_id', $user_id]])->first();

                //Debemos comprobar que un diploma para este curso y usuario ya existe
                if($diploma) {
                    //  Validamos que el archivo PDF exista
                    //  Si existe entonces
                    //      Obtenemos el PDF y lo enviamos al correo del usuario
                    //  Si no existe entonces
                    //      Devolvemos error || Creamos de nuevo el archivo PDF y lo enviamos
                    $userStoragePath = storage_path('app/public/user/'.$user->id.'/course/');
                    self::sendCertificateMail($user, $userStoragePath.$diploma->pdf_url);
                    $response->data = $userStoragePath.$diploma->pdf_url;
                    $response->code = 200;
                    $response->msg = 'Ya habia un diploma.';
                } else {
                    //Se genera el diploma y se envia el mail
                    $pdfPath = self::generateCertificatePDF($course, $user);
                    if($pdfPath) {
                        self::sendCertificateMail($user, $pdfPath);
                        //Enviar notificacion push de certificado
                        Notification::saveNotification(array('user_id' => 1, 'users' => [$user->id], 'msg' => 'Tu certificación presencial ha sido un éxito. Espera noticias del equipo sobre tu certificado.'));

                        //Desbloquea el acceso al bloque 5 y al contenido 1 del mismo;
                        UserAccess::unlockAccess(2, 5, $user->id);
                        $module = Module::where([['block_id', '=', 5], ['order', '=', 1]])->first();
                        if($module) {
                            UserAccess::unlockAccess(3, $module->id, $user->id);
                        }

                        $response->data = $pdfPath;
                        $response->code = 200;
                        $response->msg = 'PDF creado con exito.';
                    }
                }

            } else {
                $response->code = 404;
                $response->msg = "El curso o usuario no existe";
            }

        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    private static function sendCertificateMail($user, $filepath)
    {
        $subject = 'Certificación Draught Master';
        $message = "<p>Estimado {$user->name} {$user->lastname}:</p><p>Tu certificación presencial ha sido un éxito. Espera noticias del equipo sobre tu certificado.</p>";
        $optionName = 'Certificado.pdf';
        $response = false;

        try {
            if (Mail::attachmentMail($user->email, $filepath, $message, $subject, $optionName)) {
                $response = true;
            }
        } catch (Exception $e) {
            throw $e;
        }
        return $response;

    }

    private static function generateCertificatePDF(Course $course, User $user)
    {
        $response = false;

        try {
            //Obtenemos la imagen del certificado
            try {
                $image = Image::make(resource_path('assets/images/c'.$course->id.'/certificate.png'));
            } catch (\Exception $e) {
                throw new \Exception("No existe la imagen de certificado para el curso ".$course->id, 1);
            }

            //Escribimos el nombre del usuario en la imagen de certificado
            $image->text($user->name.' '.$user->lastname, 1725, 1700, function($font){
                $font->file(resource_path('assets/font/Oswald.ttf'));
                $font->size(90);
                $font->align('center');
            });
            $imageName = time().'.png';
            $userStoragePath = storage_path('app/public/user/'.$user->id.'/');
            $fullImagePath = $userStoragePath.$imageName;
            $image->stream();
            //Guardamos la imagen temporalmente
            Storage::put('user/'.$user->id.'/'.$imageName, $image, 'public');
            $image->destroy();

            //Generamos el html que contendra la imagen del certificado
            $html = self::generateCertificateHtml($fullImagePath);

            CPDF::$PAPER_SIZES['certificate'] = array(0, 0, 567.50, 792.00);
            $pdfName = 'c'.$course->id.'_certificate_'.time().'.pdf';
            //Creamos el PDF del certificado en base al HTML generado anteriormente
            $pdf = PDF::loadHTML($html)->setPaper('certificate', 'landscape')->stream();
            //Guardamos el PDF
            Storage::put('user/'.$user->id.'/course/'.$pdfName, $pdf, 'public');
            //Borramos la imagen temporal
            unlink($fullImagePath);

            //Si se creo el PDF entonces guardamos el registro en BD
            if(Storage::exists('user/'.$user->id.'/course/'.$pdfName)){
                $diploma = new Diploma();
                $diploma->course_id = $course->id;
                $diploma->user_id = $user->id;
                $diploma->pdf_url = $pdfName;
                if($diploma->save()){
                    //Devolvemos la ruta para el PDF
                    $response = $userStoragePath.'course/'.$pdfName;
                }
            }

        } catch (\Exception $e) {
            throw $e;
        }

        return $response;

    }

	public static function getUrl($course_id, $id)
	{
		$certificate = Diploma::where([['course_id', '=', $course_id], ['user_id', '=', $id]])->value('pdf_url');
		return storage_path('app/public/user/'.$id.'/course/'.$certificate);
	}

    private static function generateCertificateHtml($imagePath)
    {
        return '<html lang="en"> <head> <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <meta http-equiv="X-UA-Compatible" content="ie=edge"> <title>Certificado</title> <style type="text/css"> *{margin: 0; padding: 0;}body{height: 567.5pt}.image{width: 100%; height: 100%;}</style></head><body><img class="image" src="'.$imagePath.'" alt=""></body></html>';
    }
}
