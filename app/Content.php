<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'content';

    public function module()
    {
        return $this->belongsTo('App\Module');
    }

    public function access()
    {
        return $this->hasOne('App\UserAccess', 'object_id')->where('access_type_id', 4);
    }

    public function exam()
    {
        return $this->hasMany('App\ExamQuestion')->where('active', 1);
    }

    public static function getAll($course_id, $block_id, $module_id, $only_exams = false, $user_id = false)
    {
        $response = new Response();

        try {
            $contents = Content::where('content.active', 1);
            if($only_exams) {
                $contents->where('content.content_type_id', 4);
            }
            $contents = $contents->join('module', function($join) use ($module_id){
                $join->on('content.module_id', '=', 'module.id')->where('content.module_id', '=', $module_id);
            })
            ->join('block', function($join) use ($block_id){
                $join->on('module.block_id', '=', 'block.id')->where('module.block_id', '=', $block_id);
            })
            ->join('course', function($join) use ($course_id){
                $join->on('block.course_id', '=', 'course.id')->where('block.course_id', '=', $course_id);
            })
            ->orderBy('content.order')
            ->get(['content.*']);

            if($only_exams && $user_id) {
                $contents->load(['exam.answer' => function ($query) use ($user_id){
                    $query->join('exam_attempt', 'exam_attempt.id', '=', 'user_exam_answer.exam_attempt_id')->where('exam_attempt.user_id', '=', $user_id)
                    ->orderBy('user_exam_answer.exam_attempt_id', 'desc');
                }])->first();
            }
            $response->data = $contents;
            $response->code = 200;
        } catch (\Exception $e) {
            $response->exception = $e->getMessage().' '.$e->getLine();
        }

        return $response;

    }

    public static function get($course_id, $block_id, $module_id, $content_id)
    {
        $response = new Response();

        try {
            $contents = Content::where([
                ['content.id', $content_id],
                ['content.active', 1]
            ])
            ->join('module', function($join) use ($module_id){
                $join->on('content.module_id', '=', 'module.id')->where('content.module_id', '=', $module_id);
            })
            ->join('block', function($join) use ($block_id){
                $join->on('module.block_id', '=', 'block.id')->where('module.block_id', '=', $block_id);
            })
            ->join('course', function($join) use ($course_id){
                $join->on('block.course_id', '=', 'course.id')->where('block.course_id', '=', $course_id);
            })
            ->orderBy('content.order')
            ->get(['content.*'])->first();

            if ($contents->content_type_id === 4){
                $contents->load(['exam' => function($query) {
                    $query->where('active', 1);
                }]);
            }

            $response->data = $contents;
            $response->code = 200;
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
        }

        return $response;

    }

    public static function sendPDF($email, $course_id, $block_id, $module_id, $content_id)
    {
        $response = new Response;
        try {
            $module = Module::find($module_id);
            $filename = 'c'.$course_id.'_b'.$block_id.'_m'.$module_id.'_c'.$content_id.'.pdf';
            if (!file_exists(resource_path('pdf/'.$filename))){
                throw new \Exception("File does not exists", 1);
            }
            Mail::sendPDF($email, $filename, $module);
            $response->code = 200;
            $response->msg = "Se ha enviado el PDF a su correo.";
        }
        catch (\Exception $e) {
            $response->code = 500;
            $response->msg = "Se produjo un error al enviar el email. Inténtelo de nuevo más tarde.";
            $response->exception = $e->getMessage();
        }
        return $response;
    }

    public static function viewPDF($course_id, $block_id, $module_id, $content_id)
    {
		$pdf_path = resource_path("pdf/c{$course_id}_b{$block_id}_m{$module_id}_c{$content_id}.pdf");
        return $pdf_path;
    }

	public static function validateAnswer($content_id, $user_id, $question_id)
	{
		$response = new Response;
		try {
			// Se crea un exam attempt para referenciarlo en la respuesta del usuario
			$attempt = new ExamAttempt;
			$attempt->user_id = $user_id;
			$attempt->is_valid = 1;
			$attempt->save();
			// Se crea una respuesta y se referencía el intento de exámen.
			// Se agrega valid = 1 para que se pueda calificar sin haber subido el exámen.
			$answer = new ExamAnswer;
			$answer->exam_attempt_id = $attempt->id;
			$answer->exam_question_id = $question_id;
			$answer->is_valid = 1;
			$answer->save();
			$response->code = 200;
			$response->msg = "Se valido correctamente la respuesta";
		} catch (\Exception $e) {
			$response->code = 500;
			$response->msg = "Se produjo un error validando la respuesta, inténtelo más tarde.";
			$response->exception = $e->getMessage();
		}
		return $response;
	}
}
