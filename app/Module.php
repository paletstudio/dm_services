<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Module extends Model
{
    protected $table = 'module';

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function block()
    {
        return $this->belongsTo('App\Block');
    }

    public function access()
    {
        return $this->hasOne('App\UserAccess', 'object_id')->where('access_type_id', 3);
    }

    public static function get($course_id, $block_id, $module_id, $user_id = null)
    {
        $response = new Response();

        try {
            $modules = Module::where([
                ['module.id', $module_id],
                ['module.active', 1]
            ])
            ->join('block', function($join) use ($block_id){
                $join->on('module.block_id', '=', 'block.id')->where('module.block_id', '=', $block_id);
            })
            ->join('course', function($join) use ($course_id){
                $join->on('block.course_id', '=', 'course.id')->where('block.course_id', '=', $course_id);
            })
            ->orderBy('module.order');

            if($user_id){
                $modules = $modules->with(['access' => function($query) use ($user_id){
                    $query->where('user_id', $user_id);
                }])
                ->get(['module.*', DB::raw('(SELECT GetModuleGrade(module.id, '.$user_id.')) as grade')])->first();
            } else {
                $modules = $modules->get(['module.*'])->first();
            }

            $response->data = $modules;
            $response->code = 200;
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
        }

        return $response;

    }

    public static function getAll($course_id, $block_id, $user_id = null)
    {
        $response = new Response();

        try {
            $modules = Module::where([
                ['module.active', 1]
            ])
            ->join('block', function($join) use ($block_id){
                $join->on('module.block_id', '=', 'block.id')->where('module.block_id', '=', $block_id);
            })
            ->join('course', function($join) use ($course_id){
                $join->on('block.course_id', '=', 'course.id')->where('block.course_id', '=', $course_id);
            })
            ->orderBy('module.order');

            if($user_id){
                $modules = $modules->with(['access' => function($query) use ($user_id){
                    $query->where('user_id', $user_id);
                }])
                ->get(['module.*', DB::raw('(SELECT GetModuleGrade(module.id, '.$user_id.')) as grade')]);
            } else {
                $modules = $modules->get(['module.*']);
            }

            $response->data = $modules;
            $response->code = 200;
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
        }

        return $response;

    }

    public static function getMaxGrade($module_id, $user_id)
    {
        return DB::table('exam_attempt as ea')
        ->join('user_exam_answer as uea', 'uea.exam_attempt_id', '=', 'ea.id')
        ->join('exam_question as eq', 'eq.id', '=', 'uea.exam_question_id')
        ->join('content as c', function($join) {
            $join->on('c.id', '=', 'eq.content_id');
            // ->on('c.content_type_id', '=', 4);
        })
        ->select('ea.grade')
        ->where([['c.module_id', '=', $module_id],
            ['ea.user_id', '=', $user_id],
            ['c.content_type_id', '=', 4]
        ])
        ->orderBy('ea.grade', 'desc')
        ->first();
    }

    public static function checkModuleComplete($module_id, $user_id)
    {
        $attempts = DB::select("SELECT ea.id as attempt FROM
        content as c
        INNER JOIN exam_question as eq ON eq.content_id = c.id
        INNER JOIN user_exam_answer as a ON a.exam_question_id = eq.id
        INNER JOIN exam_attempt as ea ON ea.id = a.exam_attempt_id
        WHERE ea.user_id = ? AND c.module_id = ? AND c.content_type_id = ?
        GROUP BY ea.id ORDER BY ea.id LIMIT 1", array($user_id, $module_id, 4));
        if (count($attempts) > 0) {
            return true;
        }
        return false;
    }
}
