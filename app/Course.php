<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'course';

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function access()
    {
        return $this->hasOne('App\UserAccess', 'object_id')->where('access_type_id', 1);
    }

    public static function get($course_id, $user_id = null)
    {
        $response = new Response();

        try {
            $course = Course::find($course_id);

            $response->data = $course;

            if($user_id){
                $course->load(['access' => function($query) use ($user_id){
                    $query->where('user_id', $user_id);
                }]);
            }
            $response->code = 200;
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
        }

        return $response;

    }

    public static function getAll($user_id = null)
    {
        $response = new Response();

        try {
            $courses = Course::where('active', 1)->get();
            if($user_id){
                $courses->load(['access' => function($query) use ($user_id){
                    $query->where('user_id', $user_id);
                }]);
            }
            $response->data = $courses;
            $response->code = 200;
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
        }

        return $response;

    }
}
