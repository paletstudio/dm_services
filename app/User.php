<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;
use Storage;
use Image;
use JWTAuth;
use JWTFactory;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class User extends Authenticatable
{
    protected $table = 'user';
     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
         'name', 'lastname', 'email', 'username', 'password', 'profile_id', 'push_notif_token', 'active',
     ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'image_url',
    ];

    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }

    public function diploma()
    {
        return $this->hasMany('App\Diploma');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Authenticates the user and return a token to secure API.
     *
     * @param array $credentials User data to confirm and evaluate.
     *
     * @return Response User token for secure.
     */
    public static function login($credentials = [])
    {
        $response = new Response();

        try {
            $user = self::where([['username', '=', $credentials['username']], ['active', '=', 1]])->with(['profile', 'diploma' => function ($query) {
                $query->where('course_id', 1);
            }])->first();
            if ($user && \Hash::check($credentials['password'], $user->password)) {

				if (env('VERIFY_ORIGIN', false) == true && (($credentials['origin'] == 1 && $user->profile->is_admin !== 1) || ($credentials['origin'] == 2 && $user->profile->is_admin === 1))) {
                    $response->code = 403;
                    $response->msg = 'No tienes autorización';

                    return $response;
                }

                self::updatePushToken($user->id, $credentials['pushToken']);
                $user->last_login = new \DateTime();
                $user->save();

                $customClaims['user'] = new \stdClass();
                $customClaims['user']->id = $user->id;
                $customClaims['user']->name = $user->name;
                $customClaims['user']->lastname = $user->lastname;
                $customClaims['user']->email = $user->email;
                $customClaims['user']->diploma = false;
                if(count($user->diploma) > 0) {
                    $customClaims['user']->diploma = true;
                }
                //
                //
                // $customClaims['user']->profile = $user->profile->description;
                // $customClaims['user']->requestorigin = null;
                // $customClaims['role'] = $user->profile->name;

                $response->data = JWTAuth::fromUser($user, $customClaims);
                // $response->data = $user;
                $response->code = 200;
                $response->msg = 'Login con éxito';
            } else {
                $response->code = 401;
                $response->msg = 'Credenciales invalidas';
            }
            // $user->load('profile');
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    /**
     * Register a user.
     *
     * @param array $user User to register
     *
     * @return Response
     */
    public static function register(array $user = [])
    {
        $response = new Response();

        try {
            $newUser = new self();
			$exist = User::where('email', $user['email'])->get();
            if (empty($user)) {
                throw new \Exception("User mustn't be empty");
            }
			if (count($exist) > 0) {
				throw new \Exception('El email ingresado ya existe');
			}
            //generate a random password.
            $randPass = $newUser->randomPass();

            $newUser->name = $user['name'];
            $newUser->lastname = $user['lastname'];
            $newUser->username = $user['username'];
            $newUser->email = $user['email'];
            $newUser->city = $user['city'];
            $newUser->state = $user['state'];
            $newUser->profile_id = $user['profile_id'];
            $newUser->password = $randPass;
            $newUser->save();

            if ($newUser->id) {
                $message = "<p>Has sido invitado a certificarte como Draught Master para Heineken&#174;.</p>
                <p>Ahora debes prepararte para tu prueba de certificación a través de la APP Draught Master Training Programme, que puedes descargar desde aquí según el sistema de tu smartphone:</p>
                <p><a href='https://play.google.com/store/apps/details?id=mx.cobusiness.draughtmaster&hl=es'>Google Play</a></p>
                <p><a href='https://itunes.apple.com/us/app/the-draught-master-training-programme/id1255346130?l=es&ls=1&mt=8'>Apple Store</a></p>
				<p>O puedes ingresar directamente a la Web APP a través de <a href='http://dmapp.cobusiness.mx/'>este link</a>.</p>
				<p>Esta aplicación te guiará en los temas y las pruebas que debes completar.<br/> De aprobar esta etapa de preparación con calificación \"Star\", serás invitado a la prueba certificación presencial en Monterrey.<br/>¡Te deseamos éxito en este recorrido!</p>
				<p>Tus datos para acceder a la aplicación son:</p>
                <p><b>Usuario:</b> {$newUser->email}</p>
                <p><b>Contraseña:</b> {$randPass}</p>
				<p style=\"font-size: 80%\">*Una vez accedas a la APP, te recomendamos cambiar la contraseña por una que recuerdes más fácilmente.</p>
				<p><b>Recomendaciones Importantes:</b></p>
				<ol>
					<li>
						Separa un tiempo especial para completar cada módulo de este curso.
					</li>
					<li>
						Al usar la APP DMTP asegúrate de estar en un lugar de WiFi de alta velocidad.
					</li>
					<li>
						Descarga el manual completo <i><b>The Draugh Master Training Programme</b></i> que se encuentra en el botón <b>“START HERE”</b> en la pantalla de inicio de tu APP, y e imprímelo para facilitar tus consultas durante tu entrenamiento.
					</li>
				</ol>";
                $subject = "Bienvenido a Draught Master Training Programme";
                UserAccess::registerAccess($newUser->id);
                Mail::sendMail($newUser->email, $message, $subject);
            }
            $response->code = 200;
            $response->msg = 'El usuario ha sido creado correctamente';
            $response->data = $randPass;
        } catch (\Exception $e) {
            $response->code = 500;
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function forgotPass($email)
    {
        $response = new Response;
        try {
          $valid = User::where('email', $email)->first();
          if ($valid) {
            $date = (new \DateTime('+30 minutes'))->format('U');
            $claims = [
              'exp' => $date,
              'iat' => time(),
              'sub' => 123
            ];
			$payload = JWTFactory::make($claims);
			$token = JWTAuth::encode($payload)->get();
			$url = url('api/user/changepass')."?token=".$token."&user_id=".$valid->id;
            $message = "<p>Estimado {$valid->name} {$valid->lastname}:</p>";
            $message .= "<p>Se ha requerido un cambio de contraseña para tu cuenta de The Draught Master Training Programme. Si has sido tú, por favor, has click en el siguiente link:</p>";
            $message .= "<p><a href='".$url."'>Cambiar contraseña</a></p>";
            $message .= "<p>Si no has sido tu el que ha solicitado un cambio de contraseña, ignora éste mensaje.</p>";
            $message .= "<br><p>The Draught Master Team.</p>";
            $subject = 'Cambiar la contraseña para Draught Master';
            Mail::sendMail($valid->email, $message, $subject);
			$response->code = 200;
			$response->msg = "Se han enviado instrucciones al correo";
          } else {
            $response->code = 404;
            $response->msg = "El email ingresado no existe.";
          }
        }
        catch (\Exception $e) {
          $response->code = 500;
          $response->msg = "Se produjo un error. Por favor, inténtelo más tarde.";
          $response->exception = $e->getMessage();
        }
        return $response;
    }

	public static function sendNewPass($id)
	{
		$response = new Response;
		try {
			$user = User::find($id);
			if($user) {
				$randPass = $user->randomPass();
				$user->password = $randPass;
				if ($user->save()) {
					$subject = "Tu nueva contraseña para Draught Master Training Programme";
					$message = "<p>Estimado {$user->name} {$user->lastname}:</p>";
					$message .= "<p>Tu cambio de contraseña ha sido exitoso. Puedes entrar a la app con la siguiente contraseña:</p>";
					$message .= "<p>Contraseña: {$randPass}</p>";
					$message .= "<br><p>The Draught Master Team.</p>";
					Mail::sendMail($user->email, $message, $subject);
					$response->code = 200;
					$response->msg = "Se ha enviado un email con tu nueva contraseña.";
				}
			}
		}
		catch (\Exception $e) {
			$response->code = 500;
			$response->msg = "Se ha producido un error";
			$response->exception = $e->getMessage();
		}
		return $response;
	}

    /**
     * Get all users.
     *
     * @return Response
     */
    public static function getAll($info = false)
    {
        $response = new Response();
        try {
            if ($info) {
                $users = self::with(['diploma' => function ($query) {
                    $query->where('course_id', 1);
                }])
				->where('active', 1)
                ->get(['user.*', DB::raw('(SELECT GetUserGrade(user.id)) as grade')]);
                $modules = \App\Module::where('active', 1)->where('block_id', '<>', 6)->pluck('id');
                foreach ($users as $u) {
                    if(count($u->diploma) > 0) {
                        unset($u->diploma);
                        $u->diploma = true;
                    } else {
                        unset($u->diploma);
                        $u->diploma = false;
                    }
                    $u->module_percentage = self::getModulesPercentage($u->id, $modules);
                }
            } else {
                $users = self::where('active', 1)->get();
            }
            $response->code = 200;
            $response->data = $users;
        } catch (\Exception $e) {
            $response->code = 500;
            $response->msg = 'Se produjo un error';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    /**
     * Get a single user.
     *
     * @param int $id User id.
     *
     * @return Response
     */
    public static function get($id)
    {
        $response = new Response();
        try {
            $user = self::where('id', $id)->with(['diploma' => function ($query) {
                $query->where('course_id', 1);
            }])->first();
            if (!$user) {
                $response->code = 404;
                $response->msg = 'No existe el usuario';
            } else {
                if (count($user->diploma) > 0) {
                    unset($user->diploma);
                    $user->diploma = true;
                } else {
                    unset($user->diploma);
                    $user->diploma = false;
                }
                $response->code = 200;
                $response->data = $user;
            }
        } catch (\Exception $e) {
            $response->code = 500;
            $response->msg = 'Se produjo un error';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function getImage($id, $imagename)
    {
        $response = new \stdClass();
        $response->image = Storage::get('user/'.$id.'/course/'.$imagename);
        $response->mime = Image::make($response->image)->mime();

        return $response;
    }

    /**
     * Get profile picture from user.
     *
     * @param int $id [description]
     *
     * @return Response Image and MIME.
     */
    public static function getProfileImage($id)
    {
        $response = new \stdClass();
        try {
            $user = self::find($id);
            $imagename = $user->image_url;
            if ($imagename) {
                $response->image = Storage::get('user/'.$id.'/'.$imagename);
                $response->mime = Image::make($response->image)->mime();
            } else {
                $response->image = Image::make(resource_path('assets/images/default_pic.png'));
                $response->image->stream();
                $response->mime = $response->image->mime();
            }
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
            $response->image = false;
        }

        return $response;
    }

    /**
     * Change user profile picture.
     *
     * @param int    $id    User id.
     * @param string $image The image to upload. (Base64)
     *
     * @return Response
     */
    public static function changeProfilePicture($id, $image)
    {
        $response = new Response();
        try {
            $user = self::find($id);
            // Si el usuario tiene foto de perfil...
            if ($user && $user->image_url) {
                // Se elimina la foto anterior.
                Storage::delete("user/{$id}/$user->image_url");
            }
            // Se crea la imagen nueva.
            $img = self::createImage($image);
            $user->image_url = $img['imagename'];
            // Se guarda la imagen.
            Storage::put("user/{$id}/{$img['imagename']}", $img['img'], 'public');
            $user->save();
            $response->code = 200;
            $response->msg = 'Se guardó la imagen correctamente';
        } catch (\Exception $e) {
            $response->exception = $e->getMessage().' '.$e->getLine();
        }

        return $response;
    }

    /**
     * Changes the user password.
     *
     * @param int    $userId  User id.
     * @param string $oldPass Old password.
     * @param string $newPass New password.
     *
     * @return Response response.
     */
    public static function changePassword($userId, $oldPass, $newPass)
    {
        $response = new Response();
        try {
            $token = JWTAuth::parseToken();
            $payload = $token->getPayload();
            if ($payload['user']['id'] == $userId) {
                $user = self::where('id', $userId)->first();
                if ($user && \Hash::check($oldPass, $user->password)) {
                    $user->setPasswordAttribute($newPass);
                    if ($user->save()) {
                        $response->code = 200;
                        $response->msg = 'Se ha cambiado la contraseña exitosamente.';
                    }
                } else {
                    $response->msg = 'La contraseña no es correcta.';
                }
            } else {
                $response->code = 401;
                $response->msg = 'Acción no permitida';
            }
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function getVideo($id, $video)
    {
        $response = new \stdClass();
        $response->video = null;
        $response->code = 404;

        try {
            $video = ExamAnswer::where('answer', 'like', '%'.$video.'%')->get()->first();
            if ($video) {
                $path = $video->answer;
                if (Storage::exists($path)) {
                    $response->video = storage_path('app/public/'.$path);
                }
            }
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
            $response->code = 500;
        }

        return $response;
    }

    public static function refreshToken(){
        $response = new Response();
        $token = JWTAuth::getToken();
        if(!$token){
            $response->code = 400;
            $response->exception = 'Token not provided';
        }
        try{
            $response->data = JWTAuth::refresh($token);
            $response->code = 200;
        }catch(TokenInvalidException $e){
            $response->code = 500;
            $response->exception = $e;
            // throw new AccessDeniedHttpException('The token is invalid');
        }
        return $response;
    }

    public static function sendInvitation($id)
    {
        $response = new Response;
        try {
            $user = User::find($id);
			$filepath = resource_path('assets/images/c1/invitacion.jgp');
            $subject = 'Has sido invitado a certificarte como Draught Master para Heineken';
            $message = '<p>Hola '.$user->name.' '. $user->lastname.':</p><p>Espera que te contacte un representante del equipo para darte el detalle del tu prueba de certificación presencial.</p><br><p>- The Draught Master Training Programme - </p><img src="'.$filepath.'" alt="Invitacion draught master">';
            Mail::sendMail($user->email, $message, $subject);

            //Enviar notificacion push de Invitacion
            Notification::saveNotification(array('user_id' => 1, 'users' => [$id], 'msg' => 'DMTP te ha enviado un mensaje importante. Revisa tu correo electrónico.'));

            $response->code = 200;
            $response->msg = "Se ha enviado la invitación correctamente";
        }
        catch (\Exception $e) {
            $response->code = 500;
            $response->msg = "Se produjo un error al enviar la invitación";
            $response->exception = $e->getMessage();
        }
        return $response;
    }

	public static function deleteUser($id)
	{
		$response = new Response();
		try {
			$user = self::findOrFail($id);
			UserAccess::where('user_id', $id)->delete();
			Diploma::where('user_id', $id)->delete();
			NotificationUser::where('user_id', $id)->delete();
			Notification::where('user_id', $id)->delete();
			$attempts = ExamAttempt::where('user_id', $id)->get();
			if (count($attempts) > 0) {
				foreach ($attempts as $key => $attempt) {
					ExamAnswer::where('exam_attempt_id', $attempt->id)->delete();
					$attempt->delete();
				}
			}
			$user->delete();
			$response->code = 200;
			$response->msg = "Usuario eliminado correctamente";
		}
		catch(ModelNotFoundException $e) {
			$response->code = 404;
			$response->msg = "El usuario que desea borrar no existe";
			$response->exception = $e->getMessage();
		}
		catch (\Exception $e) {
			$response->code = 500;
			$response->msg = "Se produjo un error";
			$response->exception = $e->getMessage();
		}
		return $response;
	}

    //Private Actions

    /**
     * Generate a random password.
     *
     * @return string Rand pass.
     */
    private function randomPass()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = []; //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; ++$i) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }

        return implode($pass); //turn the array into a string
    }

    private static function updatePushToken($user_id, $token)
    {
        $response = new Response();

        try {
            $user = self::where('id', $user_id)->first();

            if ($user && $token) {
                $user->push_notif_token = $token;

                if ($user->save()) {
                    $response->data = true;
                    $response->code = 200;
                }
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    /**
     * Calculates the module progress for user.
     *
     * @param int   $user_id User id.
     * @param array $modules Modules to check.
     *
     * @return Response response object.
     */
    private static function getModulesPercentage($user_id, $modules)
    {
        $complete = 0;
        foreach ($modules as $m) {
			if ($m != 1 && $m != 14){
				if (\App\Module::checkModuleComplete($m, $user_id)) {
					$complete++;
				}
			} else {
				$complete++;
			}
        }
        $mod_length = count($modules);
        $percentage = ($complete * 100) / $mod_length;

        return $percentage;
    }

    /**
     * Retrieves an extension with MIME given.
     *
     * @param string $mime MIME type.
     *
     * @return string Extension.
     */
    private static function getExtension($mime)
    {
        $extensions = [
            'image/jpeg' => 'jpg',
            'image/png' => 'png',
            'image/gif' => 'gif',
        ];

        return $extensions[$mime];
    }

    /**
     * Create a new image. If necessary, crops the image into a square.
     *
     * @param string $img Image in Base64.
     *
     * @return array Image name and image.
     */
    private static function createImage($img)
    {
        $newImg = Image::make($img);
        $diff = 0;

        $height = $newImg->height();
        $width = $newImg->width();

        if ($width < $height) {
            $newImg->crop($width, $width);
        } elseif ($width > $height) {
            $newImg->crop($height, $height);
        }

        $mime = $newImg->mime();
        $extension = self::getExtension($mime);
        $imagename = time().'.'.$extension;
        $newImg->stream();

        return ['img' => $newImg, 'imagename' => $imagename];
    }
}
