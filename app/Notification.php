<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notification';

    public function notificationUser()
    {
        return $this->hasMany('App\NotificationUser');
    }

    public static function saveNotification($params)
    {
        $response = new Response();

        try {
            $notification = new Notification();
            $notification->user_id = $params['user_id'];
            $notification->message = $params['msg'];

            if ($notification->save()) {
                $users = [];
                if (count($params['users']) === 0) {
                    $users = User::where('profile_id', 2)->get(['id', 'push_notif_token'])->toArray();
                } else {
                    $users = User::whereIn('id', $params['users'])->get(['id', 'push_notif_token'])->toArray();
                }

                $notificationUsers = [];
                foreach ($users as $user) {
                    array_push($notificationUsers, new NotificationUser(['notification_id' => $notification->id,'user_id' => $user['id'] ]));
                }

                $notification->notificationUser()->saveMany($notificationUsers);

				if(count($params['users']) !== 0) {
					$tokens = array_map(function ($u) {
						return $u['push_notif_token'];
					}, $users);
				} else {
					$tokens = [];
				}

                self::sendPushNotification($notification->id, $params['msg'], $tokens);

                $response->code = 200;
                $response->data = true;
                $response->msg = 'Notificación enviada con éxito a todos los destinatarios';
            } else {
                $response->msg = 'Se produjo un error al guardar la notificación.';
            }
        } catch (\Exception $e) {
            $response->exception = $e->getMessage().' '.$e->getLine();
        }

        return $response;
    }

    private static function sendPushNotification($notificationId, $msg = '', $tokens = [])
    {
        $content = array(
        	"en" => $msg
        );

		$fields = array(
			'app_id' => "95c3a1cf-1d61-416e-8dd1-346eb23ba7ba",
			'contents' => $content,
			'headings' => [
				'en' => 'Draught Master Training Programme'
				],
			);

		if(count($tokens) > 0) {
			$fields['include_player_ids'] = $tokens;
		} else {
			$fields['included_segments'] = ['All'];
		}


        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                               'Authorization: Basic '.env('ONE_SIGNAL_API_KEY')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);
        curl_close($ch);
    }
}
