<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationUser extends Model
{
    protected $table = 'notification_user';

    protected $fillable = ['notification_id', 'user_id'];
}
