<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamAnswer extends Model
{
    protected $table = "user_exam_answer";

    protected $fillable = ['answer', 'grade', 'is_valid'];

    public function attempt()
    {
        return $this->belongsTo('App\ExamAttempt');
    }
    public function question()
    {
        return $this->belongsTo('\App\ExamQuestion');
    }

    public static function evaluateAnswer($answer_id, $grade)
    {
        $response = new Response();
        try {
            $answer = ExamAnswer::find($answer_id);
            $answer->grade = $grade;
            $answer->save();
            $response->code = 200;
        }
        catch (\Exception $e) {
            $response->code = 200;
            $response->exception = $e->getMessage();
            $response->msg = "Se produjo un error";
        }
        return $response;
    }
}
