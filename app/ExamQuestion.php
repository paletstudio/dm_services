<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamQuestion extends Model
{
    protected $table = 'exam_question';

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function content()
    {
        return $this->belongsTo('App\Content');
    }

    public function answer()
    {
        return $this->hasOne('App\ExamAnswer', 'exam_question_id');
    }
}
