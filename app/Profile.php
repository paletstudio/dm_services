<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile';

    protected $hidden = [
        'created_at', 'updated_at',
    ];

}
