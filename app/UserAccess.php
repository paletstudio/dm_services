<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAccess extends Model
{
    protected $table = 'user_access';

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    protected $fillable = ['user_id', 'access_type_id', 'object_id', 'locked'];

    public static function unlockAccess($access_type_id, $object_id, $user_id)
    {
        try {
            $access = UserAccess::where([['access_type_id', '=', $access_type_id], ['object_id', '=', $object_id], ['user_id', '=',  $user_id]])->first();
            if($access && $access->locked === 1) {
                $access->locked = 0;
                $access->save();

                return true;
            }
        } catch (Exception $e) {
            throw $e;
        }

        return false;
    }

    public static function doneAccess($access_type_id, $object_id, $user_id)
    {
        try {
            $access = UserAccess::where([['access_type_id', '=', $access_type_id], ['object_id', '=', $object_id], ['user_id', '=',  $user_id]])->first();
            if($access && $access->done == 0) {
                $access->done = 1;
                $access->save();

                return true;
            }
        } catch (Exception $e) {
            throw $e;
        }

        return false;
    }

    public static function registerAccess($user_id)
    {
        try {
            $inserts = UserAccess::create(['user_id' => $user_id, 'access_type_id' => 1, 'object_id' => 1, 'locked' => 0]);
            $blocks = Block::with('modules')->get();
            if ($blocks) {
                foreach ($blocks as $key => $b) {
                    if ($b->order == 1) {
                        UserAccess::create(['user_id' => $user_id, 'access_type_id' => 2, 'object_id' => $b->id, 'locked' => 0]);
                    } else {
                        UserAccess::create(['user_id' => $user_id, 'access_type_id' => 2, 'object_id' => $b->id]);
                    }

                    if ($b->modules) {
                        foreach ($b->modules as $key => $m) {
                            if ($m->order == 1) {
                                UserAccess::create(['user_id' => $user_id, 'access_type_id' => 3, 'object_id' => $m->id, 'locked' => 0]);
                            } else {
                                UserAccess::create(['user_id' => $user_id, 'access_type_id' => 3, 'object_id' => $m->id]);
                            }
                        }
                    }
                }
            }
        }
        catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Unblocks following blocks or modules.
     * @param  int $course  Course id.
     * @param  int $block   Block id.
     * @param  int $module  Module id.
     * @param  int $user_id User id.
     * @return bool
     */
    public static function unblockAccess($course = null, $block = null, $module = null, $user_id = null)
    {
        //Si alguno de los parametros no se envia o es indefinido se regresa false.
        if ($course == null || $block == null || $module == null || $user_id == null) {
            return false;
        }
        $response = "response";

        //Se seleccionan los modulos del bloque.
        $modules = (array) Module::where('block_id', $block)
		// ->select('id')
        ->orderBy('order', 'asc')
        ->pluck('id')->toArray();

        //Se declara la longitud del arreglo de modulos y la posicion del modulo enviado.
        $length = count($modules);
        $position = array_search($module, $modules);

        if ( ($position + 1) != $length ) {
            //Si el modulo enviado NO es el ultimo ...
            //Se declara el siguiente modulo
            $nextModule = $modules[$position + 1]; //position + 1, es decir, la posicion actual + 1

            /**
             * Logica para impedir que desbloquee el modulo 2 del bloque 5
             */
            // $m = Module::find($nextModule);
            // if ($m && $m->block_id == 5 && $m->order == 2) {
            //     return false;
            // }

            //Se obtienen los accesos del usuario al siguiente modulo.
            $access = UserAccess::where([
                ['access_type_id', 3],
                ['user_id', $user_id],
                ['object_id', $nextModule]
            ])
            ->first();
            //Si existe el acceso, y el acceso esta bloqueado, se desbloquea
            if ($access && $access->locked === 1) {
                $access->locked = 0;
                $access->save();
            }
            return true;
        } else {
            //Si el modulo enviado fue el ultimo del bloque...
            //Se obtienen los bloques del curso enviado.
            $blocks = Block::where('course_id', $course)
            ->select('id')
            ->orderBy('order', 'asc')
            ->get();

            //Se transforma en arreglo el objecto de bloques.
            $arrayBlocks = [];
            foreach($blocks as $b){
                array_push($arrayBlocks, $b->id);
            }

            //Se declara la longitud del arreglo de bloques y la posicion del bloque enviado.
            $length = count($arrayBlocks);
            $position = array_search($block, $arrayBlocks);
            //Si el bloque NO es el ultimo...
            if (($position + 1) != $length) {
                //Se declara el siguiente bloque
                $nextBlock = $arrayBlocks[$position + 1];

                //Se obtienen los accesos al siguiente bloque del usuario enviado.

                if($nextBlock !== 4 || $nextBlock !== 5){
                    $access = UserAccess::where([
                        ['access_type_id', 2],
                        ['user_id', $user_id],
                        ['object_id', $nextBlock]
                    ])
                    ->first();
                    //Se desbloquea el bloque
                    $access->locked = 0;
                    $access->save();
                }
                return true;
            }
        }
    }

}
