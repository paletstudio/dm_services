<?php

/****************************************************
 * This file keeps custom helper functions.
 * Feel free to add the ones you want
 ****************************************************/


/**
 * Check if an HTTP Request is from mobile.
 * @return boolean
 */
function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}
