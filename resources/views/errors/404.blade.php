<!DOCTYPE html>
<html>
    <head>
        <title>Draught Master</title>

        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #e7edf0;
                font-family: 'Lato';
				background: url('login_bg.jpg');
				background-size: cover;
				background-repeat: no-repeat;
				display: table;
            }

            .container {
                text-align: center;
				display: table-cell;
				vertical-align: center;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 50px;
                margin-bottom: 40px;
            }
			.text{
				font-size: 30px;
			}
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
				<img src="logo.png" alt="" width="400">
                <div class="title">404</div>
				<div class="text">Upss... No hemos encontrado lo que buscabas :(</div>
            </div>
        </div>
    </body>
</html>
