<!DOCTYPE html>
<html>
    <head>
        <title>Draught Master</title>

        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #e7edf0;
                font-family: 'Lato';
				background: url('login_bg.jpg');
				background-size: cover;
				background-repeat: no-repeat;
				display: table;
            }

            .container {
                text-align: center;
				display: table-cell;
				vertical-align: center;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 40px;
                margin-bottom: 40px;
            }
			p{
				font-size: 20px;
			}
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
				<img src="logo.png" alt="" width="400">
                <div class="title">¡Tu contraseña ha sido cambiada con éxito!</div>
				<p>Tu nueva contraseña ha sido enviada a tu correo.</p>
            </div>
        </div>
    </body>
</html>
